<?php
namespace App\Http\Controllers;

use Config;
use Dinero;
use Datatables;
use App\Models\Client;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\Client\StoreClientRequest;
use App\Http\Requests\Client\UpdateClientRequest;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Client\ClientRepositoryContract;
use App\Repositories\Setting\SettingRepositoryContract;
use App\ClientOptions;

class ClientsController extends Controller
{

    protected $users;
    protected $clients;
    protected $settings;

    public function __construct(
        UserRepositoryContract $users,
        ClientRepositoryContract $clients,
        SettingRepositoryContract $settings
    )
    {
        $this->users = $users;
        $this->clients = $clients;
        $this->settings = $settings;
        $this->middleware('client.create', ['only' => ['create']]);
        $this->middleware('client.update', ['only' => ['edit']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('clients.index');
    }

    /**
     * Make json respnse for datatables
     * @return mixed
     */
    public function anyData()
    {
        $clients = Client::select(['id', 'city' ,'name', 'email', 'primary_number', 'status']);
        return Datatables::of($clients)
            ->addColumn('namelink', function ($clients) {
                return '<a href="clients/' . $clients->id . '" ">' . $clients->name . '</a>';
            })
            ->add_column('status', function ($clients) { 
                return $this->status_make_html_code($clients->status);
            })
            ->add_column('edit', '
                <a href="{{ route(\'clients.edit\', $id) }}" class="btn btn-success" >Edit</a>')
            ->add_column('delete', '
                <form action="{{ route(\'clients.destroy\', $id) }}" method="POST">
            <input type="hidden" name="_method" value="DELETE">
            <input type="submit" name="submit" value="Delete" class="btn btn-danger" onClick="return confirm(\'Are you sure?\')"">

            {{csrf_field()}}
            </form>')
            ->make(true);
    }

    public function updateDocuments (Request $r, $client_id){

        $client = ClientOptions::where('client_id', $client_id)->first();
        if($client == NULL){
            $client_options_new = new ClientOptions();
            $client_options_new->passport = $r->input('passport');
            $client_options_new->ielts = $r->input('ielts');
            $client_options_new->tabelya = $r->input('tabelya');
            $client_options_new->address = $r->input('address');
            $client_options_new->photo = $r->input('photo');
            $client_options_new->motivation_letter = $r->input('motivation_letter');
            $client_options_new->rec_letter = $r->input('rec_letter');
            $client_options_new->gramoty = $r->input('gramoty');
            $client_options_new->parents_passport = $r->input('parents_passport');
            $client_options_new->attestat = $r->input('attestat');
            $client_options_new->apostil = $r->input('apostil');
            $client_options_new->perevod = $r->input('perevod');
            $client_options_new->notarius = $r->input('notarius');
            $client_options_new->client_id = $client_id;
            $client_options_new->save();
        }else{
            ClientOptions::where('client_id', $client_id)
                            ->update([
                                    "passport" =>$r->input('passport'),
                                    "ielts" =>$r->input('ielts'),
                                    "tabelya" =>$r->input('tabelya'),
                                    "address" =>$r->input('address'),
                                    "photo" =>$r->input('photo'),
                                    "motivation_letter" =>$r->input('motivation_letter'),
                                    "rec_letter" =>$r->input('rec_letter'),
                                    "gramoty" =>$r->input('gramoty'),
                                    "parents_passport" =>$r->input('parents_passport'),
                                    "attestat" =>$r->input('attestat'),
                                    "apostil" =>$r->input('apostil'),
                                    "perevod" =>$r->input('perevod'),
                                    "notarius" =>$r->input('notarius')
                                ]);
        }

        return back();
    }

    public function updateDogovor (Request $r, $client_id){

        $client = ClientOptions::where('client_id', $client_id)->first();
        if($client == NULL){
            $client_options_new = new ClientOptions();
            $client_options_new->consultatsia = $r->input('consultatsia');
            $client_options_new->dogovor = $r->input('dogovor');
            $client_options_new->documents = $r->input('documents');
            $client_options_new->apply = $r->input('apply');
            $client_options_new->paid = $r->input('paid');
            $client_options_new->finished = $r->input('finished');
            $client_options_new->paid_kind = $r->input('paid_kind');
            $client_options_new->client_id = $client_id;
            $client_options_new->save();
        }else{
            ClientOptions::where('client_id', $client_id)
                            ->update([
                                    "consultatsia" =>$r->input('consultatsia'),
                                    "dogovor" =>$r->input('dogovor'),
                                    "documents" =>$r->input('documents'),
                                    "apply" =>$r->input('apply'),
                                    "paid" =>$r->input('paid'),
                                    "finished" =>$r->input('finished'),
                                    "paid_kind" =>$r->input('paid_kind')
                                ]);
            if($r->input('dogovor')==1 || $r->input('apply') == 1 || $r->input('documents') == 1){
                Client::where('id', $client_id)->update(['status'=>2]);
            }
            if($r->input('paid')==1 || $r->input('finished') == 1){
                Client::where('id', $client_id)->update(['status'=>3]);
            }
            if($r->input('dogovor')==0 && $r->input('apply') == 0 && $r->input('documents') == 0
                && $r->input('paid')==0 && $r->input('finished') == 0){
                 Client::where('id', $client_id)->update(['status'=>1]);
            }

        }
        return back();
    }

    public function status_make_html_code($status){
        if($status == 1){
            return '<center><span class="badge" style="background:red;">&nbsp;</span></center>';
        }
        if($status == 2){
            return '<center><span class="badge" style="background:yellow;">&nbsp;</span></center>';
        }
        if($status == 3){
            return '<center><span class="badge" style="background:green;">&nbsp;</span></center>';
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        return view('clients.create')
            ->withUsers($this->users->getAllUsersWithDepartments())
            ->withIndustries($this->clients->listAllIndustries());
    }

    /**
     * @param StoreClientRequest $request
     * @return mixed
     */
    public function store(StoreClientRequest $request)
    {
        $this->clients->create($request->all());
        return redirect()->route('clients.index');
    }

    /**
     * @param Request $vatRequest
     * @return mixed
     */
    public function cvrapiStart(Request $vatRequest)
    {
        return redirect()->back()
            ->with('data', $this->clients->vat($vatRequest));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return mixed
     */
    public function show($id)
    {
        $ar['client_options'] = ClientOptions::where('client_id', $id)->first();
        if($ar['client_options'] == NULL){
            $new = new ClientOptions();
            $new->client_id = $id;
            $new->save();
        }
        $ar['client_options'] = ClientOptions::where('client_id', $id)->get();
        return view('clients.show', $ar)
            ->withClient($this->clients->find($id))
            ->withCompanyname($this->settings->getCompanyName())
            ->withInvoices($this->clients->getInvoices($id))
            ->withUsers($this->users->getAllUsersWithDepartments());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return mixed
     */
    public function edit($id)
    {
        return view('clients.edit')
            ->withClient($this->clients->find($id))
            ->withUsers($this->users->getAllUsersWithDepartments())
            ->withIndustries($this->clients->listAllIndustries());
    }

    /**
     * @param $id
     * @param UpdateClientRequest $request
     * @return mixed
     */
    public function update($id, UpdateClientRequest $request)
    {
        $this->clients->update($id, $request);
        Session()->flash('flash_message', 'Client successfully updated');
        return redirect()->route('clients.index');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $this->clients->destroy($id);

        return redirect()->route('clients.index');
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function updateAssign($id, Request $request)
    {
        $this->clients->updateAssign($id, $request);
        Session()->flash('flash_message', 'New user is assigned');
        return redirect()->back();
    }

}
