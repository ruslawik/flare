<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientOptions extends Model
{
    protected $table = "client_options";
}
