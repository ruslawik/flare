<div class="col-md-6">

    <h1 class="moveup">{{$client->name}}</h1>
      <!--Client info leftside-->

    <h3>Status
    @if($client->status == 1)
         <span class="badge" style="background:red;">&nbsp;</span>
    @endif
    @if($client->status == 2)
         <span class="badge" style="background:yellow;">&nbsp;</span>
    @endif
    @if($client->status == 3)
         <span class="badge" style="background:green;">&nbsp;</span>
    @endif
    </h3>
    @isset($client_options)
   <form action="/clients/update_dogovor_checkboxes/{{$client->id}}" method="POST">
      {{csrf_field()}}
    
    <div class="row">
        <div class="col-sm-5">
    <h4>Dogovor</h4>
     <table class="table table-striped">
            <tr><td>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="consultatsia" @if($client_options['0']->consultatsia ==1) checked @endif>consultatsia</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="dogovor" @if($client_options['0']->dogovor ==1) checked @endif>dogovor</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="documents" @if($client_options['0']->documents ==1) checked @endif>documents</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="apply" @if($client_options['0']->apply ==1) checked @endif>apply</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="paid" @if($client_options['0']->paid ==1) checked @endif>paid</label>
    </div>
    Kind of payments
    <select class="form-control" name="paid_kind">
        <option>{{$client_options['0']->paid_kind}}</option>
        <option></option>
        <option value="Cash">Cash</option>
        <option value="Bank">Bank</option>
    </select>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="finished" @if($client_options['0']->finished ==1) checked @endif>finished</label>
    </div>
    <input type="submit" value="Save" class="btn btn-success">
    </td></tr>
        </table>
    </div>
</form>
    <form action="/clients/update_documents_checkboxes/{{$client->id}}" method="POST">
      {{csrf_field()}}
    <div class="col-sm-3">
        <h4>Documents</h4>
        <table class="table table-striped">
            <tr><td>
        <div class="checkbox">
        <label><input type="checkbox" value="1" name="passport" @if($client_options['0']->passport ==1) checked @endif>passport</label>
    </div>
        <div class="checkbox">
        <label><input type="checkbox" value="1" name="ielts" @if($client_options['0']->ielts ==1) checked @endif>ielts</label>
    </div>
        <div class="checkbox">
        <label><input type="checkbox" value="1" name="tabelya" @if($client_options['0']->tabelya ==1) checked @endif>tabelya</label>
    </div>
        <div class="checkbox">
        <label><input type="checkbox" value="1" name="address" @if($client_options['0']->address ==1) checked @endif>address</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="photo" @if($client_options['0']->photo ==1) checked @endif>photo</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="motivation_letter" @if($client_options['0']->motivation_letter ==1) checked @endif>motivation letter</label>
    </div>
        </td><td>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="rec_letter" @if($client_options['0']->rec_letter ==1) checked @endif>recommendation letter</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="gramoty" @if($client_options['0']->gramoty ==1) checked @endif>gramoty/dostijeniya</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="parents_passport" @if($client_options['0']->parents_passport ==1) checked @endif>passport parents</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="attestat" @if($client_options['0']->attestat ==1) checked @endif>attestat</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="apostil" @if($client_options['0']->apostil ==1) checked @endif>apostil</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="perevod" @if($client_options['0']->perevod ==1) checked @endif>perevod</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="notarius" @if($client_options['0']->notarius ==1) checked @endif>notarius zaverit</label>
    </div>
      <input type="submit" value="Save" class="btn btn-success">
</td></tr>
        </table>
    </div>
</div>
    </form>
    @endisset
    <hr> 
  

        <div class="contactleft ">
            <table class="table"><tr>
        @if($client->email != "")
        <td>
                <!--MAIL-->
        <p><span class="glyphicon glyphicon-envelope" aria-hidden="true" data-toggle="tooltip"
                 title="{{ __('mail') }}" data-placement="left"> </span>
            <a href="mailto:{{$client->email}}" data-toggle="tooltip" data-placement="left">{{$client->email}}</a></p>
        </td>
        @endif
        @if($client->primary_number != "")
         <td>
                <!--Work Phone-->
        <p><span class="glyphicon glyphicon-phone" aria-hidden="true" data-toggle="tooltip"
                 title=" {{ __('Primary number') }} " data-placement="left"> </span>
            <a href="tel:{{$client->work_number}}">{{$client->primary_number}}</a></p>
             </td>
        @endif
        @if($client->secondary_number != "")
         <td>
                <!--Secondary Phone-->
        <p><span class="glyphicon glyphicon-phone" aria-hidden="true" data-toggle="tooltip"
                 title="{{ __('Secondary number') }}" data-placement="left"> </span>
            <a href="tel:{{$client->secondary_number}}">{{$client->secondary_number}}</a></p>
             </td>
        @endif
        @if($client->address || $client->zipcode || $client->city != "")
         <td>
                <!--Address-->
        <p><span class="glyphicon glyphicon-home" aria-hidden="true" data-toggle="tooltip"
                 title="{{ __('Full address') }}" data-placement="left"> </span> {{$client->address}}
            <br/>{{$client->zipcode}} {{$client->city}}
        </p>
         </td>
        @endif
        </tr></table>
    </div>
    <!--Client info leftside END-->
    <!--Client info rightside-->
    <div class="contactright">
        @if($client->company_name != "")
                <!--Company-->
        <p><span class="glyphicon glyphicon-star" aria-hidden="true" data-toggle="tooltip"
                 title="{{ __('Company') }}" data-placement="left"> </span> {{$client->company_name}}</p>
        @endif
        @if($client->vat != "")
                <!--Company-->
        <p><span class="glyphicon glyphicon-cloud" aria-hidden="true" data-toggle="tooltip"
                 title="{{ __('vat') }}" data-placement="left"> </span> {{$client->vat}}</p>
        @endif
        @if($client->industry != "")
                <!--Industry-->
        <p><span class="glyphicon glyphicon-briefcase" aria-hidden="true" data-toggle="tooltip"
                 title="{{ __('Industry') }}"data-placement="left"> </span> {{$client->industry}}</p>
        @endif
        @if($client->company_type!= "")
                <!--Company Type-->
        <p><span class="glyphicon glyphicon-globe" aria-hidden="true" data-toggle="tooltip"
                 title="{{ __('Company type') }}" data-placement="left"> </span>
            {{$client->company_type}}</p>
        @endif
    </div>
</div>

<!--Client info rightside END-->
