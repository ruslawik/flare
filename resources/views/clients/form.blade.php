<div class="form-group">
    {!! Form::label('name', __('Name'), ['class' => 'control-label']) !!}
    {!!
        Form::text('name',
        isset($data['owners']) ? $data['owners'][0]['name'] : null,
        ['class' => 'form-control'])
    !!}
</div>

<div class="form-group">
    {!! Form::label('email', __('Email'), ['class' => 'control-label']) !!}
    {!!
        Form::email('email',
        isset($data['email']) ? $data['email'] : null,
        ['class' => 'form-control'])
    !!}
</div>

<div class="form-group">
    {!! Form::label('address', __('Address'), ['class' => 'control-label']) !!}
    {!!
        Form::text('address',
        isset($data['address']) ? $data['address'] : null,
        ['class' => 'form-control'])
    !!}
</div>

<div class="form-group">
        {!! Form::label('city', __('City'), ['class' => 'control-label']) !!}
        {!!
            Form::text('city',
            isset($data['city']) ? $data['city'] : null,
            ['class' => 'form-control'])
        !!}
</div>

<div class="form-inline">
    <div class="form-group col-sm-6 removeleft">
        {!! Form::label('primary_number', __('Primary number'), ['class' => 'control-label']) !!}
        {!!
            Form::text('primary_number',
            isset($data['phone']) ? $data['phone'] : null,
            ['class' => 'form-control'])
        !!}
    </div>


<div class="form-group">
    {!! Form::label('user_id', __('Assign user'), ['class' => 'control-label']) !!}
    {!! Form::select('user_id', $users, null, ['class' => 'form-control ui search selection top right pointing search-select', 'id' => 'search-select']) !!}

</div>



{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
